import sys
import unittest

from bettertimes.crypto.ciphertext import CipherText
from bettertimes.protocol.multiplication_server import OutsourcingServer


class ProgressIndicatingTest(unittest.TestCase):
    def setUp(self):
        s = "Testing %s..." % self.id()
        print("\n%s%s" % (s, " " * (79 - len(s)))),
        sys.stdout.flush()


class ExtendedAsserts(object):
    def assertEquals(self, first, second, msg=None):
        self.assertEqual(first, second, msg)

    def assertEqual(self, first, second, msg=None):
        first = self.decrypt_if_needed(first)

        return super(ExtendedAsserts, self).assertEqual(first, second, msg)

    def decrypt_if_needed(self, x):
        if isinstance(x, list):
            return [
                self.scheme.decrypt(self.key_pair, bit) if isinstance(bit, CipherText) else bit
                for bit in x
                ]
        else:
            res = None
            if isinstance(x, CipherText):
                res = self.scheme.decrypt(self.key_pair, x)
            else:
                res = x

            return res % self.scheme.plaintext_space(self.key_pair)


class ExtendedAssertsProgressIndicatingTest(ExtendedAsserts, ProgressIndicatingTest):
    pass


class CryptoSchemeTest(object):
    bits = 1024

    @classmethod
    def enc(cls, p):
        return cls.scheme.encrypt(cls.key_pair, p)

    @classmethod
    def dec(cls, c):
        return cls.scheme.decrypt(cls.key_pair, c)


class PlaintextServer(OutsourcingServer):
    """ For testing purposes, we can skip encryption and decryption """

    # TODO: This hints at design errors. Perhaps encryption scheme-ciphertext mapping should be provided in the
    # TODO: constructor

    def __init__(self):
        super(PlaintextServer, self).__init__(None, None)

    def _dec(self, c):
        return c

    def _enc(self, cipher, pt):
        if not isinstance(pt, IntBit):
            return IntBit(pt)
        return pt

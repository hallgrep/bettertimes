

class AbstractPublicKey(object):
    pass


class AbstractPrivateKey(object):
    pass


class KeyPair(AbstractPublicKey, AbstractPrivateKey):
    def __init__(self, public_key, private_key):
        """
        :type public_key: bettertimes.crypto.keypair.AbstractPublicKey
        :type private_key: bettertimes.crypto.keypair.AbstractPrivateKey
        """
        self.private_key = private_key
        self.public_key = public_key

    def __getattr__(self, item):
        """ If either key in the pair has a attribute, return it """
        if item.startswith('__') and item.endswith('__'):
            return super(KeyPair, self).__getattribute__(item)

        if hasattr(self.private_key, item):
            return getattr(self.private_key, item)

        if hasattr(self.public_key, item):
            return getattr(self.public_key, item)

        raise AttributeError("%s not found in keys" % item)

    def __str__(self):
        return "%s(%s, %s)" % (self.scheme, self.public_key, self.private_key)
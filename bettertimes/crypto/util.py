import gmpy2
import random



def mul_mod(a, b, mod):
    product = gmpy2.mul(a, b)
    return gmpy2.t_mod(product, mod)


def get_rand(bits):
    return random.getrandbits(bits)


def get_prime(bits):
    """ Returns a random prime of specified size """
    max_rand = 2 ** bits
    rand = False

    # If we're unlucky, the next prime could be > max
    while not rand or rand > max_rand:
        rand = gmpy2.next_prime(get_rand(bits))
    return rand
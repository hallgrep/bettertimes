from gmpy2 import mpz

from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.elgamal import ElGamal
from bettertimes.crypto.schemes.elgamal_ecc import ElGamalECC
from bettertimes.crypto.schemes.paillier import Paillier

from bettertimes.test_util import ProgressIndicatingTest, CryptoSchemeTest


class TestAdditiveCipherText(CryptoSchemeTest, ProgressIndicatingTest):
    @classmethod
    def setUpClass(cls):
        cls.scheme = Paillier()
        cls.key_pair = cls.scheme.keygen(cls.bits)

        cls.a = 5
        cls.b = 3

        cls.enc_a = cls.enc(cls.a)
        cls.enc_b = cls.enc(cls.b)

        cls.pt = 7

    def test_add(self):
        self.assertEqual(self.dec(self.enc_a + self.enc_b), self.dec(self.enc_a) + self.dec(self.enc_b))

    def test_add_plaintext(self):
        self.assertEqual(self.dec(self.enc_a + self.b), self.a + self.b)

    def test_radd(self):
        self.assertEqual(self.dec(5 + self.enc_b), 5 + self.b)

    def test_neg(self):
        self.assertEqual(self.dec(-self.enc_b), self.key_pair.n - self.b)

    def test_sub(self):
        self.assertEqual(self.dec(self.enc_a - self.enc_b), self.a - self.b)

    def test_sub_plaintext(self):
        self.assertEqual(self.dec(self.enc_a - self.b), self.a - self.b)

    def test_sub_to_zero(self):
        self.assertEqual(self.dec(self.enc_a - self.a), 0)

    def test_rsub(self):
        self.assertEqual(self.dec(5 - self.enc_b), 5 - self.b)

    def test_mul(self):
        self.assertEqual(self.dec(self.enc_b * self.pt), self.pt * self.b)

    def test_rmul(self):
        self.assertEqual(self.dec(self.pt * self.enc_b), self.b * self.pt)

    def test_mul_mpz(self):
        setups = [(
            Paillier(),
            Paillier().keygen(self.bits)
        ), (
            ElGamal(),
            ElGamal().keygen(self.bits)
        ), (
            ElGamalECC(),
            ElGamalECC().keygen(160)
        ), (
            DGK(),
            DGK().keygen(self.bits, allow_decrypt=True)
        )]

        for scheme, keys in setups:
            self.scheme = scheme
            self.key_pair = keys
            self.assertEqual(self.dec(self.enc_b * mpz(3)), 3 * self.b)

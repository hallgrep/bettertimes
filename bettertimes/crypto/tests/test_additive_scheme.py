import random

from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.elgamal import ElGamal, additive_reverse_mapping
from bettertimes.crypto.schemes.elgamal_ecc import ElGamalECC, create_reverse_mapping
from bettertimes.test_util import ProgressIndicatingTest, CryptoSchemeTest, ExtendedAssertsProgressIndicatingTest
from bettertimes.crypto.schemes.paillier import Paillier




class AdditiveSchemeTests(CryptoSchemeTest):
    scheme = None
    key_pair = None

    @classmethod
    def setUpClass(cls):
        cls.key_pair = cls.scheme.keygen(cls.bits)
        cls.ca = cls.scheme.raw_encrypt(cls.key_pair, cls.a)
        cls.cb = cls.scheme.raw_encrypt(cls.key_pair, cls.b)

    def test_encrypt(self):
        self.assertGreater(self.enc(12345), 12345 * 3)  # At least likely so

    @classmethod
    def get_decryption_ceiling(cls):
        return cls.scheme.plaintext_space(cls.key_pair) - 1

    def test_encrypt_decrypt(self, ceiling=None):
        if ceiling is None:
            ceiling = self.get_decryption_ceiling()

        for i in range(100):
            x = random.randint(0, ceiling)
            actual = self.dec(self.enc(x)) % self.scheme.plaintext_space(self.key_pair)
            self.assertEqual(actual, x)

    def test_decrypt(self):
        self.assertEqual(self.dec(self.ca), self.a)

    def test_add(self):
        c = self.scheme.add(self.key_pair, self.ca, self.cb)
        self.assertEqual(self.dec(c), self.a + self.b)

    def test_sub(self):
        c = self.scheme.sub(self.key_pair, self.cb, self.ca)

        self.assertEqual(self.dec(c), self.b - self.a)

    def test_mul(self):
        pass

    def test_neg(self):
        c = self.scheme.neg(self.key_pair, self.ca)
        self.assertEqual(self.dec(c), self.scheme.plaintext_space(self.key_pair) - self.dec(self.ca))


class PaillierTests(AdditiveSchemeTests, ProgressIndicatingTest):
    scheme = Paillier()
    a = 3
    b = 7


class DGKTests(AdditiveSchemeTests, ExtendedAssertsProgressIndicatingTest):
    scheme = DGK()
    a = 1
    b = 2

    @classmethod
    def setUpClass(cls):
        cls.key_pair = cls.scheme.keygen(cls.bits, allow_decrypt=True, l=20)

        cls.ca = cls.scheme.raw_encrypt(cls.key_pair, cls.a)
        cls.cb = cls.scheme.raw_encrypt(cls.key_pair, cls.b)

    def test_neg(self):
        pass  # Hooray for compliance!

    @classmethod
    def dec(cls, c):
        return cls.scheme.decrypt(cls.key_pair, c)


class ElGamalTests(AdditiveSchemeTests, ProgressIndicatingTest):
    scheme = ElGamal(reverse_mapping=additive_reverse_mapping)
    a = 3
    b = 7
    bits = 2048

    @classmethod
    def get_decryption_ceiling(cls):
        return 100

    def test_no_assert_neg(self):
        # Negative values are too large to decrypt, so let's add a number to end up on the positive numbers
        c = self.scheme.neg(self.key_pair, self.ca)

    def test_neg(self):
        # Negative values are too large to decrypt, so let's add a number to end up on the positive numbers
        c = self.scheme.neg(self.key_pair, self.ca)

        cc = self.scheme.add(self.key_pair, self.cb, c)

        self.assertEqual(self.dec(cc), self.b - self.a)


class ElGamalECCTests(ElGamalTests):
    bits = 224
    scheme = ElGamalECC(reverse_mapping=create_reverse_mapping(100))

    @classmethod
    def get_decryption_ceiling(cls):
        return 99
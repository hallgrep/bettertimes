from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.outsourcing import OutsourcingDGK
from bettertimes.crypto.ciphertext import OutsourceableCipherText
from bettertimes.crypto.schemes.outsourcing import OutsourcingPaillier
from bettertimes.crypto.schemes.paillier import Paillier
from bettertimes.protocol.multiplication_client import OutsourcingClient
from bettertimes.protocol.multiplication_server import OutsourcingServer
from bettertimes.test_util import ProgressIndicatingTest
from bettertimes.crypto.tests.test_additive_scheme import AdditiveSchemeTests



class OutsourcingTests(AdditiveSchemeTests):
    a = 23
    b = 49

    @classmethod
    def setUpClass(cls):
        cls.ca = cls.enc(cls.a)
        cls.cb = cls.enc(cls.b)

    def test_encrypt(self):
        self.assertTrue(isinstance(self.enc(self.a), OutsourceableCipherText))

    def test_encrypt_decrypt(self):
        self.assertEqual(self.dec(self.enc(self.a)), self.a)

    def test_decrypt(self):
        self.assertEqual(self.dec(self.ca), self.a)

    def test_add(self):
        c = self.ca + self.cb

        self.assertEqual(self.dec(c), self.a + self.b)

    def test_sub(self):
        c = self.cb - self.ca

        self.assertEqual(self.dec(c), self.b - self.a)

    def test_neg(self):
        c = -self.ca
        self.assertEqual(self.dec(c), self.key_pair.n - self.a)

    def test_mul(self):
        cc = self.ca * self.cb
        c = self.dec(cc)
        self.assertEqual(c, self.a * self.b)


class OutsourcingPaillierTests(OutsourcingTests, ProgressIndicatingTest):
    @classmethod
    def setUpClass(cls):
        paillier = Paillier()
        cls.key_pair = paillier.keygen(cls.bits)
        outsourcing_client = OutsourcingClient(OutsourcingServer(cls.key_pair, None, paillier, None))
        cls.scheme = OutsourcingPaillier(outsourcing_client)
        cls.client = outsourcing_client

        super(OutsourcingPaillierTests, cls).setUpClass()


class OutsourcingDGKTests(OutsourcingTests, ProgressIndicatingTest):
    @classmethod
    def setUpClass(cls):
        # Setting large l makes it slow, but we can decrypt larger values
        dgk = DGK()
        cls.key_pair = dgk.keygen(cls.bits, allow_decrypt=True, l=11)
        outsourcing_client = OutsourcingClient(OutsourcingServer(cls.key_pair, None, dgk, None))
        cls.scheme = OutsourcingDGK(outsourcing_client)
        cls.client = outsourcing_client

        super(OutsourcingDGKTests, cls).setUpClass()

    def test_neg(self):
        pass
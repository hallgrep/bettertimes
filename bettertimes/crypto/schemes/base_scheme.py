from bettertimes.crypto.ciphertext import CipherText


class BaseScheme(object):
    @staticmethod
    def plaintext_space(public_key):
        raise NotImplementedError("What's your plaintext space?")

    @staticmethod
    def cipher_space(public_key):
        raise NotImplementedError("What's your ciphertext space?")

    def raw_encrypt(self, public_key, plaintext):
        raise NotImplementedError("An encryption scheme must be able to encrypt")

    def raw_decrypt(self, private_key, ciphertext):
        raise NotImplementedError("An encryption scheme must be able to decrypt")

    def keygen(self, bits):
        raise NotImplementedError("An encryption scheme must implement key generation")

    def encrypt(self, public_key, plaintext):
        return CipherText(self.raw_encrypt(public_key, plaintext), public_key, self)

    def decrypt(self, private_key, ciphertext):
        """
        :type private_key: crypto.keypair.AbstractPrivateKey
        :type ciphertext: CipherText
        :rtype:
        """
        if isinstance(ciphertext, CipherText):
            return self.raw_decrypt(private_key, ciphertext.ciphertext)

        return self.raw_decrypt(private_key, ciphertext)

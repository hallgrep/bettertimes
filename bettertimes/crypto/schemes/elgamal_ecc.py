
import gmpy2

from bettertimes.crypto.ecc import EC, Coord, \
    toy, brainpoolP160r1, brainpoolP224r1, brainpoolP192r1, brainpoolP256r1, brainpoolP512r1
from bettertimes.crypto.keypair import KeyPair
from bettertimes.crypto.schemes import elgamal


RAND = gmpy2.random_state()
DEFAULT_DECRYPTION_LIMIT = 1

# Keys are almost identical to traditional ElGamal keys, except we now need to represent the group using more than the
# number of bits of the keys.
class PrivateKey(elgamal.PrivateKey):
    pass


class PublicKey(elgamal.PublicKey):
    def __init__(self, ec, q, g, h, bits):
        self.ec = ec
        super(PublicKey, self).__init__(q, g, h, bits)


def mapping(keys, i):
    return keys.ec.mul(keys.g, i) if i >= 0 else None


def reverse_mapping(keys, c, decryption_limit=DEFAULT_DECRYPTION_LIMIT):
    for i in range(decryption_limit):
        if mapping(keys, i) == c:
            return i

    return ">%s" % (decryption_limit - 1)


def create_reverse_mapping(decryption_limit):
    return lambda keys, c: reverse_mapping(keys, c, decryption_limit)


DEFAULT_MAPPING = mapping
DEFAULT_REVERSE_MAPPING = reverse_mapping


class ElGamalECC(elgamal.ElGamal):
    def __init__(self, mapping=DEFAULT_MAPPING, reverse_mapping=DEFAULT_REVERSE_MAPPING, **kwargs):
        super(ElGamalECC, self).__init__(**kwargs)

        self.mapping = mapping
        self.reverse_mapping = reverse_mapping

    def keygen(self, bits):
        if bits == 19:
            p, a, b, gx, gy = toy()
        elif bits == 160:
            p, a, b, gx, gy = brainpoolP160r1()
        elif bits == 192:
            p, a, b, gx, gy = brainpoolP192r1()
            # p, a, b, Gx, Gy = nist_p192()
        elif bits == 224:
            p, a, b, gx, gy = brainpoolP224r1()
        elif bits == 256:
            p, a, b, gx, gy = brainpoolP256r1()
        elif bits == 512:
            p, a, b, gx, gy = brainpoolP512r1()
        else:
            raise ValueError("Unhandled key size")

        ec = EC(a, b, p)
        g = Coord(gx, gy)

        x = gmpy2.mpz_random(RAND, ec.q)
        h = ec.mul(g, x)

        public_key = PublicKey(ec, p, g, h, bits)

        return KeyPair(public_key, PrivateKey(x))

    def raw_encrypt(self, public_key, plaintext, mapping=mapping):
        """encrypt
        - plain: data as a point on ec
        - pub: pub key as points on ec
        - returns: (ciphertext1, ciphertext2) as points on ec
        """
        plaintext = self.mapping(public_key, plaintext)
        r = gmpy2.mpz_random(RAND, public_key.ec.q)

        p1 = public_key.ec.mul(public_key.g, r)
        p2 = public_key.ec.add(plaintext, public_key.ec.mul(public_key.h, r))

        return self._expand_ciphertext(p1, p2)

    def raw_decrypt(self, private_key, ciphertext):
        """decrypt
        - chiper: (chiper1, chiper2) as points on ec
        - priv: private key as int < ec.q
        - returns: plain as a point on ec
        """
        c1, c2 = self._contract_ciphertext(ciphertext)
        point = private_key.ec.add(c2, private_key.ec.neg(private_key.ec.mul(c1, private_key.x)))
        return self.reverse_mapping(private_key, point)

    def add(self, public_key, ciphertext1, ciphertext2):
        c11, c12 = self._contract_ciphertext(ciphertext1)
        c21, c22 = self._contract_ciphertext(ciphertext2)

        c1 = public_key.ec.add(c11, c21)
        c2 = public_key.ec.add(c12, c22)
        return self._expand_ciphertext(c1, c2)

    def sub(self, public_key, ciphertext1, ciphertext2):
        c11, c12 = self._contract_ciphertext(ciphertext1)
        c21, c22 = self._contract_ciphertext(ciphertext2)

        c1 = public_key.ec.sub(c11, c21)
        c2 = public_key.ec.sub(c12, c22)
        return self._expand_ciphertext(c1, c2)

    def mul_const(self, public_key, ciphertext, plaintext):
        c1, c2 = self._contract_ciphertext(ciphertext)
        return self._expand_ciphertext(public_key.ec.mul(c1, plaintext), public_key.ec.mul(c2, plaintext))

    def _expand_ciphertext(self, *args):
        # return args[0] if len(args) == 1 else args

        parts = []
        for ciphertext in args:
            parts = parts + [ciphertext.x, ciphertext.y]

        if len(parts) == 1:
            return parts[0]

        return tuple(parts)

    def _contract_ciphertext(self, *args):
        # return args[0] if len(args) == 1 else args

        ciphertexts = []
        for i in range(len(args)):
            c1 = Coord(args[i][0], args[i][1])
            c2 = Coord(args[i][2], args[i][3])

            ciphertexts.append((c1, c2))

        if len(ciphertexts) == 1:
            return ciphertexts[0]

        return ciphertexts

from bettertimes.crypto.ciphertext import OutsourceableCipherText
from bettertimes.crypto.schemes.additive_scheme import AdditiveScheme
from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.paillier import Paillier


class OutsourcingScheme(AdditiveScheme):
    def __init__(self, client, *args, **kwargs):
        super(OutsourcingScheme, self).__init__(*args, **kwargs)
        self.client = client

    def encrypt(self, public_key, plaintext):
        return OutsourceableCipherText(self.raw_encrypt(public_key, plaintext), public_key, self, self.client)

    def mul(self, a, b):
        """Multiplies an encrypted integer by a constant or another ciphertext"""
        product = self.client.mul(a, b)
        return product


class OutsourcingPaillier(OutsourcingScheme, Paillier):
    pass


class OutsourcingDGK(OutsourcingScheme, DGK):
    pass

# Can't use outsourcing for ElGamal.
#
# class OutsourcingElGamal(OutsourcingScheme, ElGamal):
#     def __init__(self, client, mapping=elgamal.DEFAULT_MAPPING, reverse_mapping=elgamal.DEFAULT_REVERSE_MAPPING, *args,
#                  **kwargs):
#         super(OutsourcingElGamal, self).__init__(client, mapping=mapping, reverse_mapping=reverse_mapping, *args, **kwargs)
#
#
# class OutsourcingElGamalECC(OutsourcingScheme, ElGamalECC):
#     def __init__(self, client, mapping=elgamal_ecc.DEFAULT_MAPPING, reverse_mapping=elgamal_ecc.DEFAULT_REVERSE_MAPPING,
#                  *args,
#                  **kwargs):
#         super(OutsourcingElGamalECC, self).__init__(client, mapping=mapping, reverse_mapping=reverse_mapping, *args, **kwargs)

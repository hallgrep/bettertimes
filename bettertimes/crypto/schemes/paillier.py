from bettertimes.crypto.keypair import AbstractPrivateKey, AbstractPublicKey
from bettertimes.crypto.util import get_prime, get_rand


import gmpy2

from bettertimes.crypto.schemes.additive_scheme import AdditiveLogScheme
from bettertimes.crypto.keypair import KeyPair


class PublicKey(AbstractPublicKey):
    def __init__(self, n, g, n_sq, bits):
        self.n = n
        self.g = g
        self.n_sq = n_sq
        self.bits = bits


class PrivateKey(AbstractPrivateKey):
    def __init__(self, l, m, p, q):
        self.l = l
        self.m = m
        self.p = p
        self.q = q


class Paillier(AdditiveLogScheme):
    def __init__(self):
        super(Paillier, self).__init__()

    @staticmethod
    def plaintext_space(public_key):
        return public_key.n

    @staticmethod
    def cipher_space(public_key):
        return public_key.n_sq

    def raw_encrypt(self, public_key, plaintext):
        r = get_rand(public_key.bits)
        x = gmpy2.powmod(r, public_key.n, public_key.n_sq)
        cipher = (gmpy2.powmod(public_key.g, plaintext, public_key.n_sq) * x) % public_key.n_sq
        return cipher

    def raw_decrypt(self, private_key, ciphertext):
        x = gmpy2.powmod(ciphertext, private_key.l, private_key.n_sq) - 1

        plain = ((x // private_key.n) * private_key.m) % private_key.n
        return plain

    def keygen(self, bits):
        p = get_prime(bits // 2)
        q = get_prime(bits // 2)

        n = gmpy2.mul(p, q)

        g = n + 1
        n_sq = n ** 2

        l = gmpy2.lcm(p - 1, q - 1)
        m = gmpy2.divm(1, l, n)

        public_key = PublicKey(n, g, n_sq, bits)
        private_key = PrivateKey(l, m, p, q)

        key_pair = KeyPair(public_key, private_key)

        return key_pair
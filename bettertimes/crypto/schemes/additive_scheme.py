import gmpy2

from bettertimes.crypto.ciphertext import AdditiveCipherText
from bettertimes.crypto.schemes.base_scheme import BaseScheme
from bettertimes.crypto.util import mul_mod


class AdditiveScheme(BaseScheme):
    def __init__(self, *argv, **kwargs):
        super(AdditiveScheme, self).__init__(*argv, **kwargs)

        if not self.encrypt:
            raise ValueError("Need to override self.encrypt")

    def encrypt(self, public_key, plaintext):
        return AdditiveCipherText(self.raw_encrypt(public_key, plaintext), public_key, self)

    def add(self, public_key, ciphertext1, ciphertext2):
        raise NotImplementedError("An additive encryption scheme must implement addition")

    def mul_const(self, private_key, ciphertext, plaintext):
        raise NotImplementedError("An additive encryption scheme must implement multiplication by plaintext")

    def neg(self, public_key, ciphertext):
        raise NotImplementedError("An additive encryption scheme must implement negation")

    def sub(self, public_key, ciphertext1, ciphertext2):
        return self.add(public_key, ciphertext1, self.neg(public_key, ciphertext2))


class AdditiveLogScheme(AdditiveScheme):
    def __init__(self, *argv, **kwargs):
        super(AdditiveLogScheme, self).__init__(*argv, **kwargs)

    def sub(self, public_key, ciphertext1, ciphertext2):
        return gmpy2.divm(ciphertext1, ciphertext2, self.cipher_space(public_key))

    def neg(self, public_key, ciphertext1):
        #return self.sub(public_key, 0, a)
        return gmpy2.divm(self.raw_encrypt(public_key, 0), ciphertext1, self.cipher_space(public_key))

    def add(self, public_key, ciphertext1, ciphertext2):
        """Add one encrypted integer to another"""
        return mul_mod(ciphertext1, ciphertext2, self.cipher_space(public_key))

    def mul_const(self, public_key, ciphertext, plaintext):
        """Multiplies an encrypted integer by a constant"""
        return gmpy2.powmod(ciphertext, plaintext, self.cipher_space(public_key))
import gmpy2

from bettertimes.crypto.keypair import AbstractPublicKey, AbstractPrivateKey
from bettertimes.crypto.util import get_prime, get_rand
from bettertimes.crypto.schemes.additive_scheme import AdditiveLogScheme
from bettertimes.crypto.keypair import KeyPair



class PublicKey(AbstractPublicKey):
    def __init__(self, n, g, h, u, l, bits):
        self.n = n
        self.g = g
        self.h = h
        self.u = u
        self.l = l
        self.bits = bits


class PrivateKey(AbstractPrivateKey):
    def __init__(self, vp, vq, p, q, decryption_table):
        self.vp = vp
        self.vq = vq
        self.p = p
        self.q = q
        self.decryption_table = decryption_table


class DGK(AdditiveLogScheme):
    def __init__(self):
        super(DGK, self).__init__()

    @staticmethod
    def cipher_space(public_key):
        return public_key.n

    @staticmethod
    def plaintext_space(public_key):
        return public_key.u

    def raw_encrypt(self, public_key, plaintext):
        r = get_rand(public_key.bits)
        x = gmpy2.powmod(public_key.h, r, public_key.n)
        g_to_plaintext = gmpy2.powmod(public_key.g, plaintext, public_key.n)

        cipher = (g_to_plaintext * x) % public_key.n
        return cipher

    def raw_decrypt(self, private_key, ciphertext):
        """
            By default, this does not actually decrypt the ciphertext, but rather
            checks whether it's the encryption of a zero or not
        """
        exp = private_key.vp * private_key.vq

        if private_key.decryption_table:
            c = gmpy2.powmod(ciphertext, exp, private_key.n)
            plain = private_key.decryption_table[c]
            return plain
        else:
            # c^v mod p == 1 iff c encrypts 0
            c_v_mod_p = int(gmpy2.powmod(ciphertext, exp, private_key.p))

            return 0 if c_v_mod_p == 1 else 1

    def forced_decryption(self, private_key, ciphertext):
        vqp = private_key.vp * private_key.vq
        wanted_c = gmpy2.powmod(ciphertext.ciphertext, vqp, private_key.n)

        m = 0
        while m < private_key.u:
            c = gmpy2.powmod(private_key.g, vqp * m, private_key.n)
            if c == wanted_c:
                return m

            m += 1

        raise ValueError("Seems I can't decrypt this ciphertext :((")

    def keygen(self, bits, allow_decrypt=False, t=160, l=2):
        """
        From stackexchange:
            http://crypto.stackexchange.com/questions/2649/dgk-cryptosystem-key-generation-and-decryption-issues
        """
        u = gmpy2.next_prime(2 ** l)
        vp = get_prime(t)
        vq = get_prime(t)

        # u and v must be zero mod p-1 and q-1
        def get_safe_prime(v):
            prime = 4
            pp = 0
            while not gmpy2.is_prime(prime):
                pp = get_prime(bits // 2)  # bits/2 because p * q = n, log2(n) = bits
                prime = 2 * pp * v * u + 1
            return prime, pp

        p, pr = get_safe_prime(vp)
        q, qr = get_safe_prime(vq)

        p1_factors = [2, pr, vp, u]
        q1_factors = [2, qr, vq, u]

        n = p * q

        gr = self.__alg483(p, q, p1_factors, q1_factors)
        hr = self.__alg483(p, q, p1_factors, q1_factors)
        h = gmpy2.powmod(hr, 2 * u * pr * qr, n)
        g = gmpy2.powmod(gr, 2 * pr * qr, n)


        public_key = PublicKey(n, g, h, u, l, bits)
        private_key = PrivateKey(vp, vq, p, q, {})

        key_pair = KeyPair(public_key, private_key)

        if allow_decrypt:
            key_pair = self.add_decryption_table(key_pair)

        return key_pair

    def add_decryption_table(self, key_pair):
        decryption_table = {}
        print("Decryption table has length %s" % key_pair.u)
        for m in range(key_pair.u):
            if m % 100 == 0:
                print("\tAt item %s/%s" % (m, key_pair.u))
            c = gmpy2.powmod(key_pair.g, key_pair.vp * key_pair.vq * m, key_pair.n)
            decryption_table[c] = m

        public_key = PublicKey(key_pair.n, key_pair.g, key_pair.h, key_pair.u, key_pair.l, key_pair.bits)
        private_key = PrivateKey(key_pair.vp, key_pair.vq, key_pair.p, key_pair.q, decryption_table)

        extended_key_pair = KeyPair(public_key, private_key)

        return extended_key_pair

    def __alg483(self, p, q, factors_p, factors_q):
        """
        INPUT: two distinct odd primes, p, q, and the factorizations of p-1 and q-1.
        OUTPUT: an element alpha of maximum order lcm(p-1;q-1) in Z*n, where n=pq.
            Use Algorithm 4.80 with G=Z*p and n=p-1 to find a generator a of Z*p.
            Use Algorithm 4.80 with G=Z*q and n=q-1 to find a generator b of Z*q.
            Use Gauss's algorithm (Algorithm 2.121) to find an integer alpha,
                1 \le alpha \le n-1, satisfying alpha = a(modp) and alpha = b(modq).
            Return alpha.
        """
        a = self.__alg480(p, factors_p)
        b = self.__alg480(q, factors_q)
        alpha = self.__alg2121([a, b], [p, q], p * q)
        return alpha

    def __alg480(self, n, factors):
        """
        INPUT: a cyclic group G of order n, and the prime factorization n
        OUTPUT: a generator alpha of G
            Choose a random element alpha in G
            For i from 1 to k do the following:
            Compute b <- an/pi (N.B. (modn))
            If b=1 then go to step 1.
            Return alpha.
        """
        bits = int(gmpy2.ceil(gmpy2.log2(n)))
        k = len(factors)
        b = 1
        alpha = 0

        while b == 1:
            alpha = get_rand(bits)
            for i in range(k):
                b = gmpy2.powmod(alpha, n // factors[i], n)
                if b == 1:
                    break
                    # If we reached the break statement, we'll run the while loop again
                    # Otherwise, we have our result in alpha.

        return alpha

    @staticmethod
    def __alg2121(a, factors, n):
        """
        The solution x to the simultaneous congruences in the Chinese Remainder Theorem
        (Fact 2.120) may be computed as sum(i=1..k)(a[i]*N[i]*M[i]) (modn), where Ni=n/ni and
        Mi=N-1i(modni). These computations can be performed in O((lgn)2) bit
        operations.

        """

        def div_n(i, n):
            return gmpy2.div(n, factors[i])

        def div_n_mod(i, n):
            return gmpy2.divm(1, div_n(i, n), factors[i])

        k = len(factors)
        x = 0
        for i in range(k):
            x += a[i] * div_n(i, n) * div_n_mod(i, n)
        return x
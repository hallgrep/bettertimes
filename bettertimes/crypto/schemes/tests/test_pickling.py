import pickle

from bettertimes.crypto.schemes.paillier import Paillier



from bettertimes.test_util import ProgressIndicatingTest


class PicklingTests(ProgressIndicatingTest):
    def test_easy_pickle(self):
        for i in range(1000):
            self.assertEqual(pickle.loads(pickle.dumps(i)), i)

    def test_tuple_pickle(self):
        for i in range(1000):
            self.assertEqual(pickle.loads(pickle.dumps((i, 'foo = %s * bar' % i))), (i, 'foo = %s * bar' % i))

    def test_paillier_key_pickle(self):
        paillier_key = Paillier().keygen(256)
        pickled_key = pickle.loads(pickle.dumps(paillier_key))

        self.assertEqual(paillier_key.n, pickled_key.n)
        self.assertEqual(paillier_key.p, pickled_key.p)
        self.assertEqual(paillier_key.q, pickled_key.q)

    def test_paillier_ciphertext_pickle(self):
        paillier = Paillier()
        key = paillier.keygen(256)
        ciphertext = paillier.encrypt(key, 3)

        pickled_ciphertext = pickle.loads(pickle.dumps(ciphertext))
        self.assertEqual(paillier.decrypt(key, pickled_ciphertext), 3)

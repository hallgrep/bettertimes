from bettertimes.crypto.keypair import AbstractPublicKey, AbstractPrivateKey
from bettertimes.crypto.keypair import KeyPair
from bettertimes.crypto.schemes.additive_scheme import AdditiveScheme
from bettertimes.crypto.util import mul_mod


import gmpy2

rand = gmpy2.random_state()


class PrivateKey(AbstractPrivateKey):
    def __init__(self, x):
        self.x = x


class PublicKey(AbstractPublicKey):
    def __init__(self, q, g, h, bits):
        self.q = q
        self.g = g
        self.h = h
        self.bits = bits


def multiplicative_mapping(keys, plaintext):
    """ Dummy mapping, for vanilla ElGamal """
    return plaintext


multiplicative_reverse_mapping = multiplicative_mapping


def additive_mapping(keys, num):
    return gmpy2.powmod(keys.g, num, keys.q)


def zero_check_additive_reverse_mapping(keys, num):
    return 0 if num == 1 else 1


def additive_reverse_mapping(keys, num):
    def gen(limit=None):
        i = 0
        while (not limit) or i < limit:
            yield i
            i += 1

    for i in gen(2 ** 16+1):
        if gmpy2.powmod(keys.g, i, keys.q) == num:
            return i

    return None


DEFAULT_MAPPING = additive_mapping
DEFAULT_REVERSE_MAPPING = zero_check_additive_reverse_mapping


class ElGamal(AdditiveScheme):
    def __init__(self, mapping=DEFAULT_MAPPING, reverse_mapping=DEFAULT_REVERSE_MAPPING, **kwargs):
        super(ElGamal, self).__init__(**kwargs)

        self.mapping = mapping
        self.reverse_mapping = reverse_mapping

    def keygen(self, bits):
        # Public
        seed = gmpy2.mpz_urandomb(rand, bits)
        q = gmpy2.next_prime(seed)
        g = gmpy2.mpz_random(rand, q)

        # Secret
        x = gmpy2.mpz_random(rand, q)

        # Also public
        h = gmpy2.powmod(g, x, q)

        # Done
        public_key = PublicKey(q, g, h, bits)
        return KeyPair(public_key, PrivateKey(x))

    @staticmethod
    def plaintext_space(public_key):
        return public_key.q

    @staticmethod
    def cipher_space(public_key):
        return public_key.q

    def raw_encrypt(self, public_key, plaintext):
        # Ephemeral key
        y = gmpy2.mpz_random(rand, public_key.q)
        s = gmpy2.powmod(public_key.h, y, public_key.q)

        # Element in the group
        mapped_element = self.mapping(public_key, plaintext)

        # The two parts of our cipher text
        c1 = gmpy2.powmod(public_key.g, y, public_key.q)
        c2 = gmpy2.t_mod(gmpy2.mul(mapped_element, s), public_key.q)

        return c1, c2

    def raw_decrypt(self, private_key, ciphertext):
        c1, c2 = ciphertext

        # We can calculate s using the private key, and use it to cancel out y
        s = gmpy2.powmod(c1, private_key.x, private_key.q)

        # This is the actual decryption
        mapped_element = gmpy2.divm(c2, s, private_key.q)

        # Map the group element to our concrete plaintext space
        return self.reverse_mapping(private_key, mapped_element)

    def add(self, public_key, ciphertext1, ciphertext2):
        # def e_add(keys, a, b):
        c1 = mul_mod(ciphertext1[0], ciphertext2[0], public_key.q)
        c2 = mul_mod(ciphertext1[1], ciphertext2[1], public_key.q)
        return c1, c2

    def mul_const(self, private_key, ciphertext, plaintext):
        # defining a shorthand for multiplying times the plaintext mod private_key.q
        mul = lambda num: gmpy2.powmod(num, plaintext, private_key.q)

        # The result is both parts of the ciphertext multiplied by the plaintext, mod private_key.q (respectively)
        return mul(ciphertext[0]), mul(ciphertext[1])

    def neg(self, public_key, ciphertext):
        return self.sub(public_key, self.raw_encrypt(public_key, 0), ciphertext)

    def sub(self, public_key, ciphertext1, ciphertext2):
        # def e_sub(keys, a, b):
        c1 = gmpy2.divm(ciphertext1[0], ciphertext2[0], public_key.q)
        c2 = gmpy2.divm(ciphertext1[1], ciphertext2[1], public_key.q)
        return c1, c2




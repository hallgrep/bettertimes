import gmpy2



class CipherText(object):
    """
    TODO: this class should be immutable
    """

    def __init__(self, ciphertext, key_pair, crypto_scheme):
        """

        :type key_pair: bettertimes.crypto.keypair.KeyPair
        :type crypto_scheme:  bettertimes.crypto.schemes.base_scheme.BaseScheme
        :return:
        """
        super(CipherText, self).__init__()

        self.crypto_scheme = crypto_scheme
        self.key_pair = key_pair
        self.ciphertext = ciphertext

    def __repr__(self):
        return str(self)

    def __str__(self):
        def trunc(string):
            return "%s...%s" % (("%s" % string)[:5], ("%s" % string)[-5:])

        if hasattr(self.key_pair, 'private_key'):
            decrypted = self.crypto_scheme.decrypt(self.key_pair, self.ciphertext)
            str_decrypted = "%s" % decrypted
            if len(str_decrypted) > 10:
                str_decrypted = trunc(str_decrypted)

            return "<D(*)=%s>" % (str_decrypted)

        return "<E(*)=%s>" % trunc(self.ciphertext)


class AdditiveCipherText(CipherText):
    def __init__(self, ciphertext, key_pair, additive_scheme, **kwargs):
        super(AdditiveCipherText, self).__init__(ciphertext, key_pair, additive_scheme)

    def create_new(self, value):
        return AdditiveCipherText(value, self.key_pair, self.crypto_scheme)

    def create_new_with_other(self, value, other):
        return self.create_new(value)

    # Arithmetic operations:
    def __add__(self, other):
        """
        :rtype: AdditiveCipherText
        """
        typesafe_other = self._assert_type(other)
        cipher = self.crypto_scheme.add(self.key_pair, self.ciphertext, typesafe_other.ciphertext)
        return self.create_new_with_other(cipher, other)

    def __radd__(self, other):
        """
        :rtype: AdditiveCipherText
        """
        return self + other

    def __neg__(self):
        """
        :rtype: AdditiveCipherText
        """
        negated_cipher = self.crypto_scheme.neg(self.key_pair, self.ciphertext)
        return self.create_new(negated_cipher)

    def __sub__(self, other):
        """
        :rtype: AdditiveCipherText
        """
        typesafe_other = self._assert_type(other)
        cipher = self.crypto_scheme.sub(self.key_pair, self.ciphertext, typesafe_other.ciphertext)
        return self.create_new_with_other(cipher, other)

    def __rsub__(self, other):
        """
        :rtype: AdditiveCipherText
        """
        typesafe_other = self._assert_type(other)
        return typesafe_other - self

    def __mul__(self, other):
        """
        :rtype: AdditiveCipherText
        """
        # TODO: could also check if we know the plaintext corresponding to this ciphertext (i.e., have the key). In that
        #  case other could be a ciphertext, and self could be turned into a plaintext
        if isinstance(other, (int, long, gmpy2.mpz().__class__)):
            cipher = self.crypto_scheme.mul_const(self.key_pair, self.ciphertext, other)
            return self.create_new_with_other(cipher, other)

        raise NotImplementedError("Can't operate on %s and %s" % (self, other))

    def __rmul__(self, other):
        """
        :rtype: AdditiveCipherText
        """
        return self * other

    # Checks that the type is AdditiveCipherText, or converts it into one of possible
    def _assert_type(self, other):
        """
        :rtype: AdditiveCipherText
        """
        if isinstance(other, AdditiveCipherText):
            return other
        elif isinstance(other, (int, long)):
            return self.crypto_scheme.encrypt(self.key_pair, other)
        elif isinstance(other, gmpy2.mpz().__class__):
            return self.crypto_scheme.encrypt(self.key_pair, long(other))

        raise NotImplementedError("Can't operate on %s and %s" % (self, other))


class OutsourceableCipherText(AdditiveCipherText):
    def __init__(self, ciphertext, outsourcing_key_pair, outsourcing_scheme, client):
        self.client = client
        super(OutsourceableCipherText, self).__init__(ciphertext, outsourcing_key_pair, outsourcing_scheme)

        self.client = client

    def create_new(self, value):
        return OutsourceableCipherText(value, self.key_pair, self.crypto_scheme, self.client)

    def __mul__(self, other):
        """
        :rtype: OutsourceableCipherText
        """
        if isinstance(other, (int, long)):
            cipher = self.crypto_scheme.mul_const(self.key_pair, self.ciphertext, other)
            result = self.__class__(cipher, self.key_pair, self.crypto_scheme, self.client)
        elif isinstance(other, AdditiveCipherText):
            result = self.crypto_scheme.mul(self, other)
        else:
            raise NotImplementedError("Can't operate on %s and %s" % (self, other))

        return result


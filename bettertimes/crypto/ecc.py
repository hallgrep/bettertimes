import datetime
import gmpy2
import time

from bettertimes.crypto.util import mul_mod

n = datetime.datetime.now()
unix_time = time.mktime(n.timetuple())
rand = gmpy2.random_state(int(1000 * unix_time))


class Coord(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


class EC(object):
    """System of Elliptic Curve"""

    def __init__(self, a, b, q):
        """elliptic curve as: (y**2 = x**3 + a * x + b) mod q
        - a, b: params of curve formula
        - q: prime number
        """
        # assert 0 < a and a < q and 0 < b and b < q and q > 2
        # assert (4 * (a ** 3) + 27 * (b ** 2))  % q != 0
        self.a = a
        self.b = b
        self.q = q
        # just as unique ZERO value representation for "add": (not on curve)
        self.zero = Coord(0, 0)

    def neg(self, p):
        """negate p
        """
        if p == self.zero:
            return p
        return Coord(p.x, self.q - p.y)

    def add(self, p1, p2):
        """<add> of elliptic curve: negate of 3rd cross point of (p1,p2) line
        >>> d = ec.add(a, b)
        >>> assert ec.add(d, ec.neg(b)) == a
        >>> assert ec.add(a, ec.neg(a)) == ec.zero
        >>> assert ec.add(a, b) == ec.add(b, a)
        >>> assert ec.add(a, ec.add(b, c)) == ec.add(ec.add(a, b), c)
        """
        # Adding by zero?
        if p1 == self.zero:
            return p2
        if p2 == self.zero:
            return p1

        # If adding with point's inverse, return zero
        if p1.x == p2.x and (p1.y != p2.y or p1.y == 0):
            # p1 + -p1 == 0
            return self.zero

        # Adding by self
        if p1.x == p2.x:
            # p1 + p1: use tangent line of p1 as (p1,p1) line
            l = mul_mod(3 * p1.x * p1.x + self.a, gmpy2.divm(1, 2 * p1.y, self.q), self.q)
        # Different point!
        else:
            l = mul_mod(p2.y - p1.y, gmpy2.divm(1, p2.x - p1.x, self.q), self.q)

        x = (l * l - p1.x - p2.x) % self.q
        y = (l * (p1.x - x) - p1.y) % self.q
        return Coord(x, y)

    def mul(self, p, n):
        return self.repeatop(p, n, lambda a, b: self.add(a, b))

    def mulinv(self, p, n):
        # TODO: does this make sense?
        return self.repeatop(p, n, lambda a, b: self.sub(a, b))

    def repeatop(self, p, n, op):
        """n times <mul> of elliptic curve
        >>> m = ec.mul(p, n)
        >>> assert ec.mul(p, 0) == ec.zero
        >>> assert m == ec.mul(n, p)
        """
        r = self.zero
        m2 = p
        # O(log2(n)) add
        while 0 < n:
            if n & 1 == 1:
                r = op(r, m2)

            n, m2 = n >> 1, op(m2, m2)

        # [ref] O(n) add
        # for i in range(n):
        #    r = self.add(r, p)
        #    pass
        return r

    def sub(self, p1, p2):
        return self.add(p1, self.neg(p2))


## Bellow are standard curves

def nist_p192():
    p = 6277101735386680763835789423207666416083908700390324961279

    a = -3
    b = int("64210519", 16)

    Gx = int("188da80e", 16)
    Gy = int("07192b95", 16)

    return p, a, b, Gx, Gy


def brainpoolP160r1():
    p = int('E95E4A5F737059DC60DFC7AD95B3D8139515620F', 16)

    a = int('340E7BE2A280EB74E2BE61BADA745D97E8F7C300', 16)
    b = int('1E589A8595423412134FAA2DBDEC95C8D8675E58', 16)

    Gx = int('BED5AF16EA3F6A4F62938C4631EB5AF7BDBCDBC3', 16)
    Gy = int('1667CB477A1A8EC338F94741669C976316DA6321', 16)

    return p, a, b, Gx, Gy


def brainpoolP192r1():
    p = int('C302F41D932A36CDA7A3463093D18DB78FCE476DE1A86297', 16)

    a = int('6A91174076B1E0E19C39C031FE8685C1CAE040E5C69A28EF', 16)
    b = int('469A28EF7C28CCA3DC721D044F4496BCCA7EF4146FBF25C9', 16)

    Gx = int('C0A0647EAAB6A48753B033C56CB0F0900A2F5C4853375FD6', 16)
    Gy = int('14B690866ABD5BB88B5F4828C1490002E6773FA2FA299B8F', 16)

    return p, a, b, Gx, Gy


def brainpoolP224r1():
    p = int('D7C134AA264366862A18302575D1D787B09F075797DA89F57EC8C0FF', 16)

    a = int('68A5E62CA9CE6C1C299803A6C1530B514E182AD8B0042A59CAD29F43', 16)
    b = int('2580F63CCFE44138870713B1A92369E33E2135D266DBB372386C400B', 16)

    Gx = int('0D9029AD2C7E5CF4340823B2A87DC68C9E4CE3174C1E6EFDEE12C07D', 16)
    Gy = int('58AA56F772C0726F24C6B89E4ECDAC24354B9E99CAA3F6D3761402CD', 16)

    return p, a, b, Gx, Gy


def brainpoolP256r1():
    p = int('A9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377', 16)

    a = int('7D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9', 16)
    b = int('26DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6', 16)

    Gx = int('8BD2AEB9CB7E57CB2C4B482FFC81B7AFB9DE27E1E3BD23C23A4453BD9ACE3262', 16)
    Gy = int('547EF835C3DAC4FD97F8461A14611DC9C27745132DED8E545C1D54C72F046997', 16)

    return p, a, b, Gx, Gy


def brainpoolP512r1():
    p = int('AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA70330871'
            '7D4D9B009BC66842AECDA12AE6A380E62881FF2F2D82C68528AA6056583A48F3', 16)

    a = int('7830A3318B603B89E2327145AC234CC594CBDD8D3DF91610A83441CAEA9863BC'
            '2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CA', 16)

    b = int('3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A7'
            '2BF2C7B9E7C1AC4D77FC94CADC083E67984050B75EBAE5DD2809BD638016F723', 16)

    Gx = int('81AEE4BDD82ED9645A21322E9C4C6A9385ED9F70B5D916C1B43B62EEF4D0098E'
             'FF3B1F78E2D0D48D50D1687B93B97D5F7C6D5047406A5E688B352209BCB9F822', 16)

    Gy = int('7DDE385D566332ECC0EABFA9CF7822FDF209F70024A57B1AA000C55B881F8111'
             'B2DCDE494A5F485E5BCA4BD88A2763AED1CA2B2FA8F0540678CD1E0F3AD80892', 16)

    return p, a, b, Gx, Gy


def toy():
    p = 19

    a = 1
    b = 18

    Gx = 7
    Gy = 8
    return p, a, b, Gx, Gy

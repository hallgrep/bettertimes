import math

from bettertimes.crypto.util import get_rand


def blind(subject):
    plaintext_space_bits = int(math.log(subject.crypto_scheme.plaintext_space(subject.key_pair), 2))
    blinding = get_rand(plaintext_space_bits)
    return blinding, subject + blinding


class BlindMultiplyRequest(object):
    def __init__(self, cipher1, cipher2):
        self.veil1 = Veil(cipher1)
        self.veil2 = Veil(cipher2)

        self.cipher1 = cipher1
        self.cipher2 = cipher2

    def get_request(self):
        return self.veil1.blinded, self.veil2.blinded

    def reveal(self, result):
        random1 = self.veil1.blinding * self.cipher2
        random2 = self.veil2.blinding * self.cipher1
        random3 = self.veil1.blinding * self.veil2.blinding

        blinding = random1 + random2 + random3

        unveiled = result - blinding

        return unveiled


class AssuranceRequest(object):
    def __init__(self, cipher1, cipher2):
        """
        Create a challenge as:
            challenge = (cipher + s) * t

        :type cipher1 bettertimes.crypto.ciphertext.AdditiveCipherText
        :type cipher2 bettertimes.crypto.ciphertext.AdditiveCipherText
        """
        plaintext_space_bits = int(math.log(cipher1.crypto_scheme.plaintext_space(cipher1.key_pair), 2))
        s = get_rand(plaintext_space_bits)
        t = get_rand(plaintext_space_bits)
        challenge = (cipher1 + s) * t
        self.challenge = challenge

        self.s = s
        self.t = t

        self.cipher1 = cipher1
        self.cipher2 = cipher2

    def get_request(self):
        return self.challenge, self.cipher1, self.cipher2

    def calc_sub(self, result):
        """
            Assumes that other cipher has been multiplied to self.challenge, and that assurance is thus
            other_cipher * challenge = other_cipher * (cipher + s) * t = result * t + other_cipher * s * t
        """
        return (result + self.cipher2 * self.s) * self.t

    def assure(self, result, verification):
        expected_zero = verification - self.calc_sub(result)

        plaintext_space_bits = int(math.log(result.crypto_scheme.plaintext_space(result.key_pair), 2))
        rand = get_rand(plaintext_space_bits)
        verified_result = result + expected_zero * rand

        return verified_result


class Veil(object):
    def __init__(self, subject):
        self.subject = subject
        self.blinding, self.blinded = blind(subject)

    def reveal(self, blinded):
        return blinded - self.blinding


class MultiplicationClient(object):
    def __init__(self, server_connection):
        self.server_connection = server_connection


class PlaintextMultiplicationClient(MultiplicationClient):
    def __init__(self, server_connection):
        super(PlaintextMultiplicationClient, self).__init__(server_connection)

    def mul(self, cipher1, cipher2):
        blind_request = BlindMultiplyRequest(cipher1, cipher2)
        response = self.server_connection.mul(*blind_request.get_request())
        revealed = blind_request.reveal(response)

        return revealed


class OutsourcingClient(MultiplicationClient):
    def __init__(self, server_connection):
        super(OutsourcingClient, self).__init__(server_connection)

    def int_to_bits(self, cipher, prototype):
        veil = Veil(cipher)
        blinded = self.server_connection.int_to_bits(veil.blinded, prototype=prototype)
        revealed = veil.reveal(blinded)
        return revealed

    def bits_to_int(self, cipher):
        veil = Veil(cipher)
        blinded = self.server_connection.bits_to_int(veil.blinded)
        return veil.reveal(blinded)

    def mul(self, cipher1, cipher2):
        blind_request = BlindMultiplyRequest(cipher1, cipher2)

        verified_request = AssuranceRequest(*blind_request.get_request())

        response = self.server_connection.assured_multiplication(*verified_request.get_request())
        verified = verified_request.assure(*response)

        revealed = blind_request.reveal(verified)

        return revealed

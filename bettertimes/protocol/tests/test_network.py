import random

from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.outsourcing import OutsourcingDGK
from bettertimes.protocol.multiplication_client import AssuranceRequest
from bettertimes.protocol.multiplication_client import OutsourcingClient, Veil, BlindMultiplyRequest
from bettertimes.protocol.multiplication_server import OutsourcingServer
from bettertimes.test_util import ExtendedAsserts
from bettertimes.test_util import ProgressIndicatingTest, PlaintextServer, CryptoSchemeTest


class VerifyMultiplyRequestTests(ProgressIndicatingTest):
    def test_multiplication_request(self):
        rs = [2, 3, 2, 3]
        x = 2
        y = 4

        mr = AssuranceRequest(x, y)
        self.assertEqual(mr.get_request(), ((x + rs[0]) * rs[1], x, y))


class VeilTests(ProgressIndicatingTest):
    def test_veil(self):
        veil = Veil(IntBit(3))
        self.assertTrue(isinstance(veil.blinded, BitwiseMixin))
        self.assertEqual(veil.blinded, 5)

        self.assertTrue(isinstance(veil.reveal(veil.blinded), BitwiseMixin))
        self.assertEqual(veil.reveal(veil.blinded), 3)


class MultiplyBlindRequestTests(ProgressIndicatingTest):
    def test_request(self):
        request = BlindMultiplyRequest(IntBit(3), IntBit(4))

        b1, b2 = request.get_request()
        self.assertTrue(isinstance(b1, BitwiseMixin))
        self.assertEqual(b1, 5)

        self.assertTrue(isinstance(b2, BitwiseMixin))
        self.assertEqual(b2, 6)


class ClientServerTests(ProgressIndicatingTest):
    @classmethod
    def setUpClass(cls):
        cls.server = PlaintextServer()
        cls.client = OutsourcingClient(cls.server)

        cls.prototype = IntBit()
        cls.int_prototype = IntBit()

    def test_mul(self):
        for i in range(100):
            a = random.randint(0, 100)
            b = random.randint(0, 100)
            self.assertEqual(self.client.mul(self.prototype.make(a), self.prototype.make(b)), a * b)


class OutsourcingClientServerTests(ExtendedAsserts, CryptoSchemeTest, ClientServerTests):
    @classmethod
    def setUpClass(cls):
        cls.key_pair = DGK().keygen(cls.bits, allow_decrypt=True, l=1125)
        cls.scheme = OutsourcingDGK(OutsourcingClient(OutsourcingServer(cls.key_pair, None)))
        cls.server = OutsourcingServer(cls.key_pair, None)
        cls.client = OutsourcingClient(cls.server)

        cls.prototype = FHEBit(cls.scheme.encrypt(cls.key_pair, 0))

    def assertEqual(self, a, b):
        if isinstance(b, (int, long)):
            return super(OutsourcingClientServerTests, self).assertEqual(a, b % self.scheme.plaintext_space(self.key_pair))

        return super(OutsourcingClientServerTests, self).assertEqual(a, b)

from bettertimes.bitwise.bitvector import BitVector


class MultiplicationServer(object):
    def __init__(self, int_keys, bit_keys, int_scheme, bit_scheme):
        self.int_scheme = int_scheme
        self.bit_scheme = bit_scheme
        self.int_keys = int_keys
        self.bit_keys = bit_keys

    def raw_multiply(self, cipher1, cipher2):
        """ Returns E( D(@cipher1) * D(@cipher2) ) """
        plain1 = self._dec(cipher1)
        plain2 = self._dec(cipher2)
        result = plain1 * plain2
        return self._enc(cipher1, result)

    def _dec(self, cipher):
        """ Helper function meant for internal usage, for brevity. Decrypts the supplied ciphertext """
        return cipher.crypto_scheme.decrypt(self._get_key(cipher), cipher)

    def _get_key(self, cipher):
        """ Helper function meant for internal usage, for brevity. Gets the private key associated with the supplied
        plaintext """
        if isinstance(cipher.crypto_scheme, self.int_scheme.__class__):
            return self.int_keys
        else:
            return self.bit_keys

    def _enc(self, cipher, pt):
        """ Helper function meant for internal usage, for brevity. Encrypts the given plaintext using supplied key """
        return cipher.crypto_scheme.encrypt(self._get_key(cipher), pt)


class PlaintextMultiplicationServer(MultiplicationServer):
    def __init__(self, int_keys, bit_keys, int_scheme, bit_scheme):
        super(PlaintextMultiplicationServer, self).__init__(int_keys, bit_keys, int_scheme, bit_scheme)

    def mul(self, cipher1, cipher2):
        return self.raw_multiply(cipher1, cipher2)


class OutsourcingServer(MultiplicationServer):
    """
        The server in this context is usually Alice. Alice initially requests information from Bob, but in order to
        fulfill the request he needs to query Alice for additional results
    """

    def __init__(self, int_keys, bit_keys, int_scheme, bit_scheme):
        super(OutsourcingServer, self).__init__(int_keys, bit_keys, int_scheme, bit_scheme)

    def assured_multiplication(self, challenge1, blinded1, blinded2):
        """ Faithfully runs the multiply-verify part for Alice """
        result = self.raw_multiply(blinded1, blinded2)
        assurance = self.raw_multiply(challenge1, blinded2)

        return result, assurance

    def int_to_bits(self, cipher, prototype=None):
        """ Turns a ciphertext containing an integer into it's corresponding bit vector """
        prototype = prototype or cipher

        # This is the plaintext bits corresponding to the encrypted bit vector
        dec = self._dec(cipher)
        decrypted_bits = BitVector([prototype.make(0)]).int_to_bits(dec)

        # Encrypt the bits and return them
        encrypted_bits = [prototype.make(self._enc(cipher, pt)) for pt in decrypted_bits]

        return BitVector(encrypted_bits)

    def bits_to_int(self, cipher):
        """ Returns the integer represented by the supplied encrypted bit vector representing """
        # Just decrypt every bit and leverage BitVector's bits-to-int conversion
        return BitVector.bits_to_int([self._dec(bit) for bit in cipher.bits])

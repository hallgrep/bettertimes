from bettertimes.bitwise.bitvector import BitVector
from bettertimes.crypto.ciphertext import CipherText
from bettertimes.protocol.comparison import AbstractComparisonProtocol


class AbstractDGKCompare(AbstractComparisonProtocol):
    pass


class DGKCompareClient(AbstractDGKCompare):
    def __init__(self, *args):
        super(DGKCompareClient, self).__init__(*args)
        self.s = 0

    def create_e_array(self, d_array, r, size=None):
        """
        :type d_array: [crypto.ciphertext.AdditiveCipherText]
        :type r: int
        :type size: int
        :rtype: [crypto.ciphertext.AdditiveCipherText]
        """
        # To avoid the case d = r, we append differing bits to both d and r. We compare the values 2*d + 1 and 2*r
        r_array = BitVector.int_to_bits(r, size) + [0]
        d_array = d_array + [1]

        w_array = []
        dlen = len(d_array)
        for i in range(dlen):
            w_sum = self.dgk_enc(0)
            for j in range(dlen - i - 1):
                w_sum += self.xor(d_array[j], r_array[j])

            w_array = [w_sum] + w_array

        e_array = []
        self.s = 1 - 2 * self.rand_bits(1)  # s is a random choice of {1, -1}
        for i in range(dlen):
            sign = self.s + 3 * w_array[i]
            diff = d_array[i] - r_array[i]
            e_array.append(diff + sign)

        return e_array

    def get_result(self, outcome):
        if self.s == -1:
            return not outcome
        else:
            return outcome

    def xor(self, a, b):
        """
        :type a: crypto.ciphertext.AdditiveCipherText
        :type b: int
        :rtype: crypto.ciphertext.AdditiveCipherText
        """
        return a + b - a * b * 2


class DGKCompareServer(AbstractDGKCompare):
    def __init__(self, *args):
        super(DGKCompareServer, self).__init__(*args)
        self.sharing_blinds = []

    def create_d_array(self, to_split, size):
        if isinstance(to_split, CipherText):
            to_split = self.dgk_dec(to_split)

        bits = BitVector.int_to_bits(to_split, size)
        encrypted_bits = [self.dgk_enc(bit) for bit in bits]
        return encrypted_bits

    def create_secret_sharing(self, to_split, size):
        bits = BitVector.int_to_bits(to_split)
        bits.reverse()

        if len(bits) > size:
            raise ValueError("Number to split %s is too large (must be less than %s)" % (to_split, size))

        secret_sharing = []
        self.sharing_blinds = []
        for i in range(size):
            bit = bits[i] if i < len(bits) else 0
            blinding = self.rand_bits(1)
            blinded_bit = (bit + blinding) % 2
            secret_sharing.append(self.dgk_enc(blinded_bit))

            self.sharing_blinds.append(blinding)

        return secret_sharing

    def remove_blinding(self, blinded_array):
        if len(blinded_array) != len(self.sharing_blinds):
            raise ValueError("Length of blinded array is not the same as the cached blinds saved. Either you did " +
                             "bad calculations, or used this object with concurrent secret sharings.")

        result = []
        for i in range(len(blinded_array)):
            blinded = self.dgk_dec(blinded_array[i])
            blinding = self.sharing_blinds[i]
            un_blinded = blinded - blinding
            result.append(int(un_blinded % 2))

        result.reverse()
        return result

    def get_outcome(self, array):
        for elem in array:
            if self.dgk_dec(elem) == 0:
                return True

        return False

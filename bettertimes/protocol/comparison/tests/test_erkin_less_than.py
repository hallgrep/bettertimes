import random

from bettertimes.crypto.schemes.paillier import Paillier
from bettertimes.protocol.comparison import AbstractComparisonProtocol
from bettertimes.protocol.comparison.erkin_less_than import ErkinLessThanClient, ErkinLessThanServer
from bettertimes.test_util import ProgressIndicatingTest


class AbstractErkinLessThanTest(ProgressIndicatingTest):
    bits = None
    max = None
    paillier_keys, dgk_keys = None, None

    @classmethod
    def setUpClass(cls):
        cls.bits = 1024
        cls.max_bits = 5
        cls.max = 2 ** cls.max_bits

        cls.paillier_keys, cls.dgk_keys = AbstractComparisonProtocol.generate_keys(cls.bits)

        cls.crypto = Paillier()
        cls.used_keys = cls.paillier_keys

    def dec(self, x):
        return self.crypto.decrypt(self.used_keys, x)

    def enc(self, x):
        return self.crypto.encrypt(self.used_keys, x)


class TestErkinLessThanClient(AbstractErkinLessThanTest):
    @classmethod
    def setUpClass(cls):
        super(TestErkinLessThanClient, cls).setUpClass()
        cls.client = ErkinLessThanClient(cls.paillier_keys, cls.dgk_keys, cls.max)

    def test_create_request(self):
        a = 3
        b = 5

        result = self.client.create_request(self.enc(a), self.enc(b))

        self.assertTrue(self.dec(result) > a)

    def test_get_result(self):
        a = 3
        b = 5

        self.client.create_request(self.enc(a), self.enc(b))
        result = self.client.get_possible_result(self.enc(a))


class TestErkinLessThanServer(AbstractErkinLessThanTest):
    @classmethod
    def setUpClass(cls):
        super(TestErkinLessThanServer, cls).setUpClass()
        cls.server = ErkinLessThanServer(cls.paillier_keys, cls.dgk_keys, cls.max)

    def test_mod_server(self):
        for i in range(100):
            a = random.randint(0, self.max / 2) * 2

            expected = self.enc(a % self.server.max)
            actual = self.server.mod_server(self.enc(a))

            self.assertEqual(self.dec(actual), self.dec(expected))


class TestErkinLessThan(AbstractErkinLessThanTest):
    @classmethod
    def setUpClass(cls):
        super(TestErkinLessThan, cls).setUpClass()

        cls.server = ErkinLessThanServer(cls.paillier_keys, cls.dgk_keys, cls.max)
        cls.client = ErkinLessThanClient(cls.paillier_keys, cls.dgk_keys, cls.max)

    def test_simple(self):
        a = 3
        b = 4

        result1 = self.compare(a, b)
        result2 = self.compare(b, a)
        result3 = self.compare(a, a)
        result4 = self.compare(b, b)

        self.assertEqual(result1, True)
        self.assertEqual(result2, False)
        self.assertEqual(result3, False)
        self.assertEqual(result4, False)

    def test_auto(self):
        for i in range(100):
            a = random.randint(0, self.max / 2) * 2
            b = random.randint(0, self.max / 2) * 2

            expected = a < b
            actual = self.compare(a, b)

            self.assertEqual(actual, expected)

    def compare(self, a, b):
        print("%s < %s ?" % (a, b))
        e_a = self.enc(a)
        e_b = self.enc(b)

        request = self.client.create_request(e_a, e_b)
        response = self.server.mod_server(request)
        possible_result = self.client.get_possible_result(response)

        d_array = self.server.create_d_array(response)
        e_array = self.client.create_e_array(d_array)

        outcome = self.server.get_outcome(e_array)
        comparison = self.client.get_result(outcome)

        if comparison:
            result = self.client.adjust_result(possible_result)
        else:
            result = possible_result

        return self.dec(result) == 0

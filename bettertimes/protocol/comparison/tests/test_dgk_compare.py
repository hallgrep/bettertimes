import random

from bettertimes.bitwise.bitvector import BitVector
from bettertimes.protocol.comparison.dgk_compare import AbstractDGKCompare, DGKCompareServer
from bettertimes.protocol.comparison.dgk_compare import DGKCompareClient
from bettertimes.test_util import ProgressIndicatingTest


class AbstractDGKCompareTest(object):
    def __init__(self, *args, **kwargs):
        super(AbstractDGKCompareTest, self).__init__(*args, **kwargs)

    @classmethod
    def setUpClass(cls):
        cls.bits = 1024
        cls.max = 2 ** 8 - 1

        cls.pk, cls.dgkk = AbstractDGKCompare.generate_keys(cls.bits)


class TestDGKCompareServer(AbstractDGKCompareTest, ProgressIndicatingTest):
    def setUp(self):
        self.server = DGKCompareServer(self.pk, self.dgkk, self.max)

    def test_create_secret_sharing(self):
        r = random.randint(3, 13)
        shares = self.server.create_secret_sharing(r, 4)
        self.assertEqual(len(shares), 4)

    def test_create_secret_sharing_and_remove_blinding(self):
        for r in range(0, 16):
            shares = self.server.create_secret_sharing(r, 4)

            bits_of_r = BitVector.int_to_bits(r, 4)

            revealed = self.server.remove_blinding(shares)

            self.assertEqual(bits_of_r, revealed)

    def test_get_result(self):
        for i in range(150):
            r = random.randint(0, 15)
            shares = self.server.create_secret_sharing(r, 4)
            actual = self.server.get_outcome(shares)
            expected = 0 in [self.server.dgk_dec(b) for b in shares]
            self.assertEqual(actual, expected, msg="failed for r=%s,shares=%s" % (r, shares))

    def test_too_large_number(self):
        self.server.create_secret_sharing(255, 8)  # This does not raise an exception
        self.assertRaises(ValueError, self.server.create_secret_sharing, 256, 8)  # but this does

    def test_concurrent_shares(self):
        share_1 = self.server.create_secret_sharing(127, 7)
        share_2 = self.server.create_secret_sharing(255, 8)

        orig_2 = self.server.remove_blinding(share_2)
        self.assertRaises(ValueError, self.server.remove_blinding, share_1)


class TestDGKCompareClient(AbstractDGKCompareTest, ProgressIndicatingTest):
    def setUp(self):
        self.client = DGKCompareClient(self.pk, self.dgkk, self.max)

    def test_xor(self):
        for i in range(100):
            a = random.randint(0, 1)
            b = random.randint(0, 1)

            actual = self.client.dgk_dec(self.client.xor(self.client.dgk_enc(a), b))
            expected = a ^ b

            self.assertEqual(actual, expected)

    def test_create_e_array(self):
        # TODO: this is a weak test. Is there a better one?
        size = 4
        a = random.randint(0, 2 ** size - 1)
        b = BitVector.int_to_bits(random.randint(0, 100), size)

        d_array = [self.client.dgk_enc(bit) for bit in b]
        e_array = self.client.create_e_array(d_array, a, size)

        self.assertEqual(len(e_array), len(d_array) + 1)


class TestDGKCompare(AbstractDGKCompareTest, ProgressIndicatingTest):
    def setUp(self):
        self.client = DGKCompareClient(self.pk, self.dgkk, self.max)
        self.server = DGKCompareServer(self.pk, self.dgkk, self.max)

    def test_compare(self):
        for i in range(99):
            size = 8
            a = random.randint(0, 2 ** size - 1)
            b = random.randint(0, 2 ** size - 1)

            d_array = self.server.create_d_array(a, size)
            e_array = self.client.create_e_array(d_array, b, size)

            outcome = self.server.get_outcome(e_array)
            actual = self.client.get_result(outcome)
            expected = a < b

            self.assertEqual(expected, actual)

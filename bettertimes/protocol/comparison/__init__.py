import gmpy2
import time

from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.paillier import Paillier


class AbstractComparisonProtocol(object):
    random_state = gmpy2.random_state(int(time.time() * 1000))

    def __init__(self, paillier_key_pair, dgk_key_pair, max_plaintext):
        """
        :type paillier_key_pair: crypto.keypair.KeyPair
        :type dgk_key_pair: crypto.keypair.KeyPair
        :type max_plaintext: int
        """
        super(AbstractComparisonProtocol, self).__init__()

        self.dgk_key_pair = dgk_key_pair
        self.paillier_key_pair = paillier_key_pair

        self.max = max_plaintext
        self.max_bits = int(gmpy2.log2(self.max))

        self.paillier = Paillier()
        self.dgk = DGK()

    @classmethod
    def rand_bits(cls, num_bits):
        return gmpy2.mpz_urandomb(cls.random_state, num_bits)

    @staticmethod
    def generate_keys(security_parameter):
        """
        :type security_parameter: int
        """

        paillier = Paillier()
        dgk = DGK()

        paillier_key_pair = paillier.keygen(security_parameter)
        dgk_key_pair = dgk.keygen(security_parameter, allow_decrypt=True, l=8)

        return paillier_key_pair, dgk_key_pair

    def paillier_enc(self, pt):
        return self.paillier.encrypt(self.paillier_key_pair, pt)

    def paillier_dec(self, ct):
        return self.paillier.decrypt(self.paillier_key_pair, ct)

    def dgk_enc(self, pt):
        return self.dgk.encrypt(self.dgk_key_pair, pt)

    def dgk_dec(self, ct):
        return self.dgk.decrypt(self.dgk_key_pair, ct)

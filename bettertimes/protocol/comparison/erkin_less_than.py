import gmpy2

from bettertimes.protocol.comparison import AbstractComparisonProtocol
from bettertimes.protocol.comparison.dgk_compare import DGKCompareClient, DGKCompareServer


class AbstractErkinLessThan(AbstractComparisonProtocol):
    def __init__(self, *args, **kwargs):
        super(AbstractErkinLessThan, self).__init__(*args, **kwargs)

    def enc(self, x):
        return self.paillier_enc(x)

    def dec(self, x):
        return self.paillier_dec(x)

    def plaintext_space(self):
        return self.paillier.plaintext_space(self.paillier_key_pair.public_key)


class ErkinLessThanClient(AbstractErkinLessThan):
    def __init__(self, *args, **kwargs):
        super(ErkinLessThanClient, self).__init__(*args, **kwargs)
        self.r = None
        self.z = None
        self.r_hat = None

        self.comp_client = DGKCompareClient(self.paillier_key_pair, self.dgk_key_pair, self.max)

    def create_request(self, e_a, e_b):
        """
        :type e_a: crypto.ciphertext.AdditiveCipherText
        :type e_b: crypto.ciphertext.AdditiveCipherText
        """

        added = e_a + self.max
        e_z = added - e_b
        self.z = e_z

        num_bits = int(self.max_bits + 10 + 1)
        self.r = int(gmpy2.mpz_urandomb(self.random_state, num_bits))
        d = e_z + self.r

        return d

    def get_r_hat(self):
        return self.r_hat

    def get_possible_result(self, d_hat):
        self.r_hat = (self.r % self.plaintext_space()) % self.max

        z_hat = d_hat - self.r_hat

        # print("Allll right.")
        # print("z is %s" % self.z)
        # print("d_hat is %s" % (d_hat))
        # print("r is %s, r_hat is %s" % (self.r, self.r_hat))
        # print("z %% 2^l is either %s or %s" % (z_hat, z_hat + self.max))
        # print("\t\t%s OR %s" % (BitVector.int_to_bits(self.dgk_dec(z_hat), self.max_bits), BitVector.int_to_bits(self.dgk_dec(z_hat + self.max), self.max_bits)))
        # print("result is either %s or %s" % (self.z - z_hat, self.z - (z_hat + self.max)))

        return self.z - z_hat

    def adjust_result(self, result):
        return result - self.max

    def compare(self, e_a, e_b):
        """
        :type e_a: crypto.ciphertext.AdditiveCipherText
        :type e_b: crypto.ciphertext.AdditiveCipherText
        """
        # z_l = [2l] * [a] * [b]^(-1)
        # z_l = 0 <=> a < b
        added = e_a + self.max
        e_z = added - e_b

        the_bit = self.private_mod_client(e_z, self.max)
        z_l = the_bit * 2

        return z_l

    def private_mod_client(self, x, mod):
        """
        :type x: crypto.ciphertext.AdditiveCipherText
        :type mod: int
        :rtype; crypto.ciphertext.AdditiveCipherText
        """
        num_bits = int(self.max_bits + 112 + 1)
        r = int(gmpy2.mpz_urandomb(self.random_state, num_bits))
        d = x + r

        d_hat = self.mod_server(d, mod)
        r_hat = r % mod

        z_hat = d_hat - r_hat
        return z_hat

    def create_e_array(self, d_array):
        return self.comp_client.create_e_array(d_array, self.r_hat, self.max_bits)

    def get_result(self, outcome):
        return self.comp_client.get_result(outcome)


class ErkinLessThanServer(AbstractErkinLessThan):
    def __init__(self, *args, **kwargs):
        super(ErkinLessThanServer, self).__init__(*args, **kwargs)

        self.comp_server = DGKCompareServer(self.paillier_key_pair, self.dgk_key_pair, self.max)

    def mod_server(self, x):
        result = self.dec(x) % self.max

        return self.enc(result)

    def create_d_array(self, response):
        return self.comp_server.create_d_array(self.dgk_enc(self.dec(response)), self.max_bits)

    def get_outcome(self, e_array):
        return self.comp_server.get_outcome(e_array)

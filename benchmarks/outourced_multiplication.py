import random
import sys
from time import time

from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.outsourcing import OutsourcingDGK
from bettertimes.protocol.multiplication_client import OutsourcingClient, PlaintextMultiplicationClient
from bettertimes.protocol.multiplication_server import OutsourcingServer, PlaintextMultiplicationServer
from bettertimes.test_util import ExtendedAsserts


class Bencher(ExtendedAsserts):
    def __init__(self, key_pair, scheme, label):
        """
        :type key_pair: crypto.keypair.KeyPair 
        :type scheme: crypto.scheme.FHEScheme
        :type label: str
        :return:
        """
        self.key_pair = key_pair
        self.scheme = scheme
        self.dynamic_label = label

        self.stamps = {}
        self.counts = {}

        self.t1 = None

        self.plaintext_space = self.scheme.plaintext_space(self.key_pair)

    def bench(self):
        x = random.randint(0, self.plaintext_space)
        y = random.randint(0, self.plaintext_space)

        cx = self.encrypt(x)
        cy = self.encrypt(y)

        self.start()
        cxy = cx * cy
        self.stop("%s: encrypted multiplication")

        expected = x * y % self.plaintext_space
        actual = self.decrypt(cxy)
        if expected != actual:
            raise ValueError("%s does not equal %s" % (expected, actual))

    def start(self):
        self.t1 = time()

    def stop(self, label):
        t2 = time()

        local_label = label % self.dynamic_label

        total = self.stamps.get(local_label, 0) + t2 - self.t1
        self.stamps[local_label] = total
        self.counts[local_label] = self.counts.get(local_label, 0) + 1

    def dump(self):
        for stamp in self.stamps:
            print("%s | total: %s\tavg: %s" % (stamp, self.stamps[stamp], self.stamps[stamp] / self.counts[stamp]))

    def encrypt(self, x):
        return self.scheme.encrypt(self.key_pair, x)

    def decrypt(self, plaintext):
        return self.scheme.decrypt(self.key_pair, plaintext)


def generate_dgk_keys(bits, l):
    t = time()
    print("Generating %s bit keys..." % (bits))
    key_pair = DGK().keygen(bits, allow_decrypt=False, l=l)
    print("Done in t=%s" % (time() - t))

    t = time()
    print("Creating decryption table for  %s plaintexts" % 2 ** l)
    key_pair = DGK().add_decryption_table(key_pair)
    print("Done in t=%s" % (time() - t))

    return key_pair


def pt(key_pair):
    scheme = OutsourcingDGK(PlaintextMultiplicationClient(PlaintextMultiplicationServer(key_pair, None)))
    label = " Naive DGK"
    return scheme, label


def ct(key_pair):
    scheme = OutsourcingDGK(OutsourcingClient(OutsourcingServer(key_pair, None)))
    label = "Secure DGK"
    return scheme, label


def main():
    a = [1024, 2048]
    ls = [2, 8, 16, 24]
    types = [ct, pt]

    for bits in a:
        for l in ls:
            key_pair = generate_dgk_keys(bits, l)
            sys.stdout.flush()

            for t in types:
                scheme, label = t(key_pair)
                b = Bencher(key_pair, scheme, "%s [%s, %02d]" % (label, bits, l))
                for i in range(0, 100):
                    b.bench()
                b.dump()
                sys.stdout.flush()


main()

import random

from bettertimes.crypto.schemes.dgk import DGK
from bettertimes.crypto.schemes.outsourcing import OutsourcingPaillier, OutsourcingDGK
from bettertimes.crypto.schemes.paillier import Paillier
from bettertimes.protocol.multiplication_client import OutsourcingClient
from bettertimes.protocol.multiplication_client import PlaintextMultiplicationClient
from bettertimes.protocol.multiplication_server import OutsourcingServer
from bettertimes.protocol.multiplication_server import PlaintextMultiplicationServer
from bettertimes.test_util import ExtendedAssertsProgressIndicatingTest

bits = 1024

import time

stamps = []


def tearDownModule():
    """ This is called when all unit tests are completed """
    global stamps
    w = 80
    dash = ":-:-"
    dashes = dash * (w / len(dash))

    print("\n\n%s\n" % dashes)

    sort = sorted(stamps, key=lambda x: x[0])
    for stamp in sort:
        const = 10
        label = "%s" % stamp[0]
        time = "%s" % stamp[1]
        time = "%s%s" % (time, " " * (17 - len(time)))
        dot = "."
        dots = dot * ((w - len(label) - len(time) - const) // len(dot))
        dots = " " * (w - const - len(label) - len(time) - len(dots)) + dots

        print("%s %s %s seconds" % (label, dots, time))

    print("\n%s\n\n" % dashes)


def timestamped(label, repeat=10):
    """ This is a decorator, to annotate any test case that we want to time stamp """

    def test_decorator(fn):
        def test_decorated(self, *args, **kwargs):
            global stamps
            total = 0

            for i in range(repeat):
                print("."),
                import sys

                sys.stdout.flush()
                t = time.time()
                stamp = []

                def stop():
                    stamp.append(time.time())

                self.stop = stop
                fn(self, *args, **kwargs)

                stamp = stamp[0] if stamp else time.time()

                local_label = label
                if hasattr(self.__class__, 'dynamic_label'):
                    local_label = label % self.__class__.dynamic_label

                total += stamp - t

            avg = total / repeat
            stamps.append((local_label, avg))

        return test_decorated

    return test_decorator


class BenchmarkAdditiveHomomorphic(object):
    @classmethod
    def c(cls, p):
        return cls.scheme.encrypt(cls.key_pair, p)

    @classmethod
    def d(cls, c):
        return cls.scheme.decrypt(cls.key_pair, c)

    @timestamped("%s: encryption")
    def test_encrypt(self):
        cx = self.c(self.x)
        self.stop()

        # TODO: bad test. Perhaps look at number of bits
        self.assertNotEqual(self.x, cx.ciphertext)

    @timestamped("%s: decryption")
    def test_decrypt(self):
        d = self.d(self.cx)
        self.stop()
        self.assertEquals(self.x, d)

    @timestamped("%s: addition")
    def test_add(self):
        cxy = self.cx + self.cy
        self.stop()

        self.assertEquals((self.x + self.y), self.d(cxy))

    @timestamped("%s: subtraction")
    def test_sub(self):
        cxy = self.cx - self.cy
        self.stop()

        self.assertEquals((self.x - self.y), self.d(cxy))

    # def test_sub_extra(self):
    # x = 3
    # y = 2
    #     cx = self.c(x)
    #     cy = self.c(y)
    #
    #     ccdiff = self.d(cx - cy)
    #     cdiff = self.d(cx - y)
    #     diff = x - y
    #
    #     self.assertEquals(ccdiff, x - y)
    #     self.assertEquals(cdiff, x - y)
    #
    #     self.assertEquals(self.d(2 - cy), 0)
    #     self.assertEquals(self.d(2 - self.c(1)), 1)

    @timestamped("%s: multiplication by constant")
    def test_mul_const(self):
        cxy = self.cx * self.y
        self.stop()

        self.assertEquals((self.x * self.y), self.d(cxy))

    @timestamped("%s: encrypted multiplication")
    def test_mul(self):
        cxy = self.cx * self.cy
        self.stop()

        self.assertEquals(self.x * self.y, self.d(cxy))


class BenchmarkDGK(ExtendedAssertsProgressIndicatingTest, BenchmarkAdditiveHomomorphic):
    dynamic_label = "DGK"

    @classmethod
    def setUpClass(cls):
        global bits
        cls.bits = bits
        cls.key_pair = DGK().keygen(cls.bits, allow_decrypt=True, l=16)
        cls.scheme = OutsourcingDGK(OutsourcingClient(OutsourcingServer(cls.key_pair, None)))

        cls.x = random.randint(0, cls.scheme.plaintext_space(cls.key_pair))
        cls.y = random.randint(0, cls.scheme.plaintext_space(cls.key_pair))

        cls.cx = cls.c(cls.x)
        cls.cy = cls.c(cls.y)


class BenchmarkNaiveDGK(ExtendedAssertsProgressIndicatingTest, BenchmarkAdditiveHomomorphic):
    dynamic_label = "Naive DGK"

    @classmethod
    def setUpClass(cls):
        global bits
        cls.bits = bits
        cls.key_pair = DGK().keygen(cls.bits, allow_decrypt=True, l=16)
        cls.scheme = OutsourcingDGK(PlaintextMultiplicationClient(PlaintextMultiplicationServer(cls.key_pair, None)))

        cls.x = random.randint(0, cls.scheme.plaintext_space(cls.key_pair))
        cls.y = random.randint(0, cls.scheme.plaintext_space(cls.key_pair))

        cls.cx = cls.c(cls.x)
        cls.cy = cls.c(cls.y)


class BenchmarkPaillier(ExtendedAssertsProgressIndicatingTest, BenchmarkAdditiveHomomorphic):
    dynamic_label = "Paillier"

    @classmethod
    def setUpClass(cls):
        global bits
        cls.bits = bits
        cls.key_pair = Paillier().keygen(cls.bits)
        cls.scheme = OutsourcingPaillier(OutsourcingClient(OutsourcingServer(cls.key_pair, None)))

        cls.x = random.randint(0, cls.scheme.plaintext_space(cls.key_pair))
        cls.y = random.randint(0, cls.scheme.plaintext_space(cls.key_pair))

        cls.cx = cls.scheme.encrypt(cls.key_pair, cls.x)
        cls.cy = cls.scheme.encrypt(cls.key_pair, cls.y)

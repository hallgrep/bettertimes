
Prerequisites
=============

BetterTimes in only compatible with python 2.7, and has not been ported to work
also with python 3.

The BetterTimes library depends on gmpy2, which depends on gmp, mpfr and
libmpc. To install them, you can for instance run one of

    brew install gmp mpfr libmpc
    apt-get install libgmp-dev libmpfr-dev libmpc-dev
    yum install gmp-devel mpfr-devel libmpc-devel

On ubuntu, you may also need to run
    
    apt-get install python-dev

Once you have the above prerequisits, you need to install gmpy2. This can be
done via pip. If you don't have pip with your python distribution, you can
install it via something like `apt-get install pip`.  After which you can
install gmpy2 through:

    pip install gmpy2


Running tests
=============

Use nosetests to run all tests

    pip install nose
    nosetests # This may take some time


Installing
==========

Want to use bettertimes for your own project? Install it using

    python setup.py install

Then, in your own project directory, you can import it as e.g. `from
bettertimes.crypto.schemes.paillier import Paillier`.


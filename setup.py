#!/usr/bin/env python

from distutils.core import setup

setup(name='bettertimes',
      version='0.1',
      description='Extension of additively homomorphic cryptosystems for outsourced multiplications by Hallgren, Ochoa and Sabelfeld',
      author='Per Hallgren',
      author_email='per.zut@gmail.com',
      packages=['bettertimes',
                'bettertimes.crypto',
                'bettertimes.crypto.schemes',
                'bettertimes.protocol.comparison',
                'bettertimes.protocol',
                'bettertimes.test_util'
      ],
      requires=['gmpy2'],
)

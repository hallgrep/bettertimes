import random


class Op(object):
    pass


class SUB(Op):
    pass


class MUL(Op):
    pass


class ADD(Op):
    pass


class NO_OP(Op):
    pass


class Ins(object):
    def __init__(self, op, o1=None, o2=None):
        super(Ins, self).__init__()

        if o1 is None:
            self.o1 = op
            self.op = NO_OP
            self.o2 = None
        else:
            self.op = op
            self.o1 = o1
            self.o2 = o2

    def __iter__(self):
        return [self.op, self.o1, self.o2].__iter__()

    def __getitem__(self, item):
        return [self.op, self.o1, self.o2][item]

    def __repr__(self):
        if self.op == NO_OP:
            return "%s" % self.o1
        return "%s %s %s" % (self.o1, self.op, self.o2)


class MulProto(object):
    def mul_verify(self, o1, o2, A):
        return o1 * o2

    def bin_op(self, op, o1, o2, A):
        if op == NO_OP:
            return o1
        else:
            o1 = self.bin_op(o1[0], o1[1], o1[2], A)
            o2 = self.bin_op(o2[0], o2[1], o2[2], A)
            if op == ADD:
                return o1 + o2
            elif op == SUB:
                return o1 - o2
            else:
                return self.mul_verify(o1, o2, A)

    def evaluate(self, alg):
        A = 0
        res = self.bin_op(alg[0], alg[1], alg[2], A)
        return res + A


class EukDist(MulProto):
    def __init__(self, xA, xB, yA, yB):
        super(EukDist, self).__init__()
        self.xA = xA
        self.xB = xB
        self.yA = yA
        self.yB = yB

    @property
    def euc_dist(self):
        return Ins(
                SUB,
                Ins(ADD,
                    Ins(ADD, Ins(self.xA * self.xA), Ins(self.yA * self.yA)),
                    Ins(ADD, Ins(MUL, Ins(self.xB), Ins(self.xB)), Ins(MUL, Ins(self.yB), Ins(self.yB)))),
                Ins(ADD,
                    Ins(MUL, Ins(MUL, Ins(self.xA), Ins(self.xB)), Ins(2)),
                    Ins(MUL, Ins(MUL, Ins(self.yA), Ins(self.yB)), Ins(2))
                    )
        )


class Pierre(EukDist):
    def __init__(self, xA, xB, yA, yB):
        super(Pierre, self).__init__(xA, xB, yA, yB)

    def prox(self):
        Dr = self.evaluate(self.euc_dist)
        return tuple([random.randint(0, 256) * (Dr - i) for i in range(3)])


xA = 1
xB = 3
yA = 1

yB = 6

dist = EukDist(xA, xB, yA, yB)
print(dist.evaluate(dist.euc_dist))
posA = xA ** 2 + yA ** 2
posB = xB ** 2 + yB ** 2
neg = (2 * xA * xB + 2 * yA * yB)

print(posA + posB - neg)

print(Pierre(xA, xB, yA, yB).prox())
